</head>

<body class='ui_mars' data-page='projects:blob:show'>
<!-- Ideally this would be inside the head, but turbolinks only evaluates page-specific JS in the body. -->
<script>
  window.project_uploads_path = "/gitlab-org/gitlab-ce/uploads";
  window.markdown_preview_path = "/gitlab-org/gitlab-ce/markdown_preview";
</script>

<header class='header-expanded navbar navbar-fixed-top navbar-gitlab'>
<div class='container'>
<div class='header-logo'>
<a class="home" data-placement="bottom" data-toggle="tooltip" href="/" id="js-shortcuts-home" title="Dashboard"><img alt="Logo white" src="/assets/logo-white-8741ca66242e138fc2e3efead1e2d7c3.png" />
<h3>GitLab</h3>
</a></div>
<div class='header-content'>
<h1 class='title'>
<span><a href="/groups/gitlab-org">GitLab.org</a> / <a href="/gitlab-org/gitlab-ce">GitLab Community Edition</a></span>
</h1>
<button class='navbar-toggle'>
<span class='sr-only'>Toggle navigation</span>
<i class="fa fa-bars"></i>
</button>
<div class='navbar-collapse collapse'>
<ul class='nav navbar-nav pull-right'>
<li class='hidden-sm hidden-xs'>
<div class='search'>
<form accept-charset="UTF-8" action="/search" class="navbar-form pull-left" method="get"><div style="display:none"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<input class="search-input form-control" id="search" name="search" placeholder="Search in this project" type="search" />
<input id="group_id" name="group_id" type="hidden" />
<input id="project_id" name="project_id" type="hidden" value="13083" />
<input id="search_code" name="search_code" type="hidden" value="true" />
<input id="repository_ref" name="repository_ref" type="hidden" value="master" />

<div class='search-autocomplete-opts hide' data-autocomplete-path='/search/autocomplete' data-autocomplete-project-id='13083' data-autocomplete-project-ref='master'></div>
</form>

</div>
<script>
  $('.search-input').on('keyup', function(e) {
    if (e.keyCode == 27) {
      $('.search-input').blur()
    }
  })
</script>

</li>
<li class='visible-sm visible-xs'>
<a data-placement="bottom" data-toggle="tooltip" href="/search" title="Search"><i class="fa fa-search"></i>
</a></li>
<li class='hidden-xs'>
<a data-placement="bottom" data-toggle="tooltip" href="/help" title="Help"><i class="fa fa-question-circle fa-fw"></i>
</a></li>
<li>
<a data-placement="bottom" data-toggle="tooltip" href="/explore" title="Explore"><i class="fa fa-globe fa-fw"></i>
</a></li>
<li>
<a data-placement="bottom" data-toggle="tooltip" href="/s/feilongklw" title="Your snippets"><i class="fa fa-clipboard fa-fw"></i>
</a></li>
<li class='hidden-xs'>
<a data-placement="bottom" data-toggle="tooltip" href="/projects/new" title="New project"><i class="fa fa-plus fa-fw"></i>
</a></li>
<li>
<a data-placement="bottom" data-toggle="tooltip" href="/profile" title="Profile settings"><i class="fa fa-cog fa-fw"></i>
</a></li>
<li>
<a class="logout" data-method="delete" data-placement="bottom" data-toggle="tooltip" href="/users/sign_out" rel="nofollow" title="Sign out"><i class="fa fa-sign-out"></i>
</a></li>
</ul>
</div>
</div>
</div>
</header>


<div class='page-sidebar-expanded page-with-sidebar'>

<div class='sidebar-wrapper'>
<ul class='project-navigation nav nav-sidebar'>
<li class="home"><a class="shortcuts-project" data-placement="right" href="/gitlab-org/gitlab-ce" title="Project"><i class="fa fa-dashboard fa-fw"></i>
<span>
Project
</span>
</a></li><li class="active"><a class="shortcuts-tree" data-placement="right" href="/gitlab-org/gitlab-ce/tree/master" title="Files"><i class="fa fa-files-o fa-fw"></i>
<span>
Files
</span>
</a></li><li class=""><a class="shortcuts-commits" data-placement="right" href="/gitlab-org/gitlab-ce/commits/master" title="Commits"><i class="fa fa-history fa-fw"></i>
<span>
Commits
</span>
</a></li><li class=""><a class="shortcuts-network" data-placement="right" href="/gitlab-org/gitlab-ce/network/master" title="Network"><i class="fa fa-code-fork fa-fw"></i>
<span>
Network
</span>
</a></li><li class=""><a class="shortcuts-graphs" data-placement="right" href="/gitlab-org/gitlab-ce/graphs/master" title="Graphs"><i class="fa fa-area-chart fa-fw"></i>
<span>
Graphs
</span>
</a></li><li class=""><a data-placement="right" href="/gitlab-org/gitlab-ce/milestones" title="Milestones"><i class="fa fa-clock-o fa-fw"></i>
<span>
Milestones
</span>
</a></li><li class=""><a class="shortcuts-issues" data-placement="right" href="/gitlab-org/gitlab-ce/issues" title="Issues"><i class="fa fa-exclamation-circle fa-fw"></i>
<span>
Issues
<span class='count issue_counter'>704</span>
</span>
</a></li><li class=""><a class="shortcuts-merge_requests" data-placement="right" href="/gitlab-org/gitlab-ce/merge_requests" title="Merge Requests"><i class="fa fa-tasks fa-fw"></i>
<span>
Merge Requests
<span class='count merge_counter'>43</span>
</span>
</a></li><li class=""><a data-placement="right" href="/gitlab-org/gitlab-ce/labels" title="Labels"><i class="fa fa-tags fa-fw"></i>
<span>
Labels
</span>
</a></li></ul>

<div class='collapse-nav'>
<a class="toggle-nav-collapse" href="#" title="Open/Close"><i class="fa fa-angle-left"></i></a>

</div>
<a class="sidebar-user" href="/u/feilongklw"><img alt="User activity" class="avatar avatar s32" src="https://secure.gravatar.com/avatar/b46b852804b3d61cfd6f85bb5df5b027?s=60&amp;d=identicon" />
<div class='username'>
feilongklw
</div>
</a></div>
<div class='content-wrapper'>
<div class='container-fluid'>
<div class='content'>
<div class='flash-container'>
</div>

<div class='clearfix'>
<div class='tree-ref-holder'>
<form accept-charset="UTF-8" action="/gitlab-org/gitlab-ce/refs/switch" class="project-refs-form" method="get"><div style="display:none"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<select class="project-refs-select select2 select2-sm" id="ref" name="ref"><optgroup label="Branches"><option value="5-4-stable">5-4-stable</option>
<option value="6-0-stable">6-0-stable</option>
<option value="6-1-stable">6-1-stable</option>
<option value="6-2-stable">6-2-stable</option>
<option value="6-3-stable">6-3-stable</option>
<option value="6-4-stable">6-4-stable</option>
<option value="6-5-stable">6-5-stable</option>
<option value="6-6-stable">6-6-stable</option>
<option value="6-7-stable">6-7-stable</option>
<option value="6-8-stable">6-8-stable</option>
<option value="6-9-stable">6-9-stable</option>
<option value="7-0-stable">7-0-stable</option>
<option value="7-1-stable">7-1-stable</option>
<option value="7-10-stable">7-10-stable</option>
<option value="7-11-stable">7-11-stable</option>
<option value="7-12-stable">7-12-stable</option>
<option value="7-13-stable">7-13-stable</option>
<option value="7-2-stable">7-2-stable</option>
<option value="7-3-stable">7-3-stable</option>
<option value="7-4-stable">7-4-stable</option>
<option value="7-5-stable">7-5-stable</option>
<option value="7-6-stable">7-6-stable</option>
<option value="7-7-stable">7-7-stable</option>
<option value="7-8-stable">7-8-stable</option>
<option value="7-9-stable">7-9-stable</option>
<option value="api-groups">api-groups</option>
<option value="auth-icons-labels">auth-icons-labels</option>
<option value="build_script">build_script</option>
<option value="cernvcs/gitlab-ce-feature/auto_link_ldap_omniauth">cernvcs/gitlab-ce-feature/auto_link_ldap_omniauth</option>
<option value="chef-docker">chef-docker</option>
<option value="comment-box-changes">comment-box-changes</option>
<option value="definition-of-done">definition-of-done</option>
<option value="dgerhardt/gitlab-ce-fix-ext-issue-tracker-hook">dgerhardt/gitlab-ce-fix-ext-issue-tracker-hook</option>
<option value="drop-satellites">drop-satellites</option>
<option value="email-full-url">email-full-url</option>
<option value="even-faster-search">even-faster-search</option>
<option value="hierarchical-navigation">hierarchical-navigation</option>
<option value="identity-validation">identity-validation</option>
<option selected="selected" value="master">master</option>
<option value="notification_on_project_moving">notification_on_project_moving</option>
<option value="participants-table">participants-table</option>
<option value="placeholder_text">placeholder_text</option>
<option value="profile-settings">profile-settings</option>
<option value="project-plus-icon">project-plus-icon</option>
<option value="quote_string">quote_string</option>
<option value="rc-announce">rc-announce</option>
<option value="refactor-css">refactor-css</option>
<option value="refactor-mr-form">refactor-mr-form</option>
<option value="remove_rubyracer">remove_rubyracer</option>
<option value="revert-mr-state-names">revert-mr-state-names</option>
<option value="road-to-rspec-3">road-to-rspec-3</option>
<option value="rs-current-user-null-object">rs-current-user-null-object</option>
<option value="rs-pipeline-groups">rs-pipeline-groups</option>
<option value="sidebar-nav">sidebar-nav</option>
<option value="sidebar-version-changelog">sidebar-version-changelog</option>
<option value="text">text</option>
<option value="themaze75/gitlab-ce-feature-session-expire-seconds-ui">themaze75/gitlab-ce-feature-session-expire-seconds-ui</option>
<option value="update-rugments">update-rugments</option>
<option value="upvotes-refactoring">upvotes-refactoring</option>
<option value="userpage-activity-scroll">userpage-activity-scroll</option></optgroup><optgroup label="Tags"><option value="v7.13.0.rc2">v7.13.0.rc2</option>
<option value="v7.13.0.rc1">v7.13.0.rc1</option>
<option value="v7.12.2">v7.12.2</option>
<option value="v7.12.1">v7.12.1</option>
<option value="v7.12.0">v7.12.0</option>
<option value="v7.12.0.rc3">v7.12.0.rc3</option>
<option value="v7.12.0.rc2">v7.12.0.rc2</option>
<option value="v7.12.0.rc1">v7.12.0.rc1</option>
<option value="v7.11.4">v7.11.4</option>
<option value="v7.11.3">v7.11.3</option>
<option value="v7.11.2">v7.11.2</option>
<option value="v7.11.1">v7.11.1</option>
<option value="v7.11.0">v7.11.0</option>
<option value="v7.11.0.rc2">v7.11.0.rc2</option>
<option value="v7.11.0.rc1">v7.11.0.rc1</option>
<option value="v7.10.5">v7.10.5</option>
<option value="v7.10.4">v7.10.4</option>
<option value="v7.10.3">v7.10.3</option>
<option value="v7.10.2">v7.10.2</option>
<option value="v7.10.1">v7.10.1</option>
<option value="v7.10.0">v7.10.0</option>
<option value="v7.10.0.rc8">v7.10.0.rc8</option>
<option value="v7.10.0.rc7">v7.10.0.rc7</option>
<option value="v7.10.0.rc6">v7.10.0.rc6</option>
<option value="v7.10.0.rc5">v7.10.0.rc5</option>
<option value="v7.10.0.rc4">v7.10.0.rc4</option>
<option value="v7.10.0.rc3">v7.10.0.rc3</option>
<option value="v7.10.0.rc2">v7.10.0.rc2</option>
<option value="v7.10.0.rc1">v7.10.0.rc1</option>
<option value="v7.9.4">v7.9.4</option>
<option value="v7.9.3">v7.9.3</option>
<option value="v7.9.2">v7.9.2</option>
<option value="v7.9.1">v7.9.1</option>
<option value="v7.9.0">v7.9.0</option>
<option value="v7.9.0.rc3">v7.9.0.rc3</option>
<option value="v7.9.0.rc2">v7.9.0.rc2</option>
<option value="v7.9.0.rc1">v7.9.0.rc1</option>
<option value="v7.8.4">v7.8.4</option>
<option value="v7.8.3">v7.8.3</option>
<option value="v7.8.2">v7.8.2</option>
<option value="v7.8.1">v7.8.1</option>
<option value="v7.8.0">v7.8.0</option>
<option value="v7.8.0.rc6">v7.8.0.rc6</option>
<option value="v7.8.0.rc5">v7.8.0.rc5</option>
<option value="v7.8.0.rc4">v7.8.0.rc4</option>
<option value="v7.8.0.rc3">v7.8.0.rc3</option>
<option value="v7.8.0.rc2">v7.8.0.rc2</option>
<option value="v7.8.0.rc1">v7.8.0.rc1</option>
<option value="v7.7.2">v7.7.2</option>
<option value="v7.7.1">v7.7.1</option>
<option value="v7.7.0">v7.7.0</option>
<option value="v7.7.0.rc4">v7.7.0.rc4</option>
<option value="v7.7.0.rc3">v7.7.0.rc3</option>
<option value="v7.7.0.rc2">v7.7.0.rc2</option>
<option value="v7.7.0.rc1">v7.7.0.rc1</option>
<option value="v7.6.2">v7.6.2</option>
<option value="v7.6.1">v7.6.1</option>
<option value="v7.6.0">v7.6.0</option>
<option value="v7.6.0.rc1">v7.6.0.rc1</option>
<option value="v7.5.3">v7.5.3</option>
<option value="v7.5.2">v7.5.2</option>
<option value="v7.5.1">v7.5.1</option>
<option value="v7.5.0">v7.5.0</option>
<option value="v7.5.0.rc1">v7.5.0.rc1</option>
<option value="v7.4.5">v7.4.5</option>
<option value="v7.4.4">v7.4.4</option>
<option value="v7.4.3">v7.4.3</option>
<option value="v7.4.2">v7.4.2</option>
<option value="v7.4.1">v7.4.1</option>
<option value="v7.4.0">v7.4.0</option>
<option value="v7.4.0.rc1">v7.4.0.rc1</option>
<option value="v7.3.3">v7.3.3</option>
<option value="v7.3.2">v7.3.2</option>
<option value="v7.3.1">v7.3.1</option>
<option value="v7.3.0">v7.3.0</option>
<option value="v7.3.0.rc1">v7.3.0.rc1</option>
<option value="v7.2.3">v7.2.3</option>
<option value="v7.2.2">v7.2.2</option>
<option value="v7.2.1">v7.2.1</option>
<option value="v7.2.0">v7.2.0</option>
<option value="v7.2.0.rc5">v7.2.0.rc5</option>
<option value="v7.2.0.rc4">v7.2.0.rc4</option>
<option value="v7.2.0.rc3">v7.2.0.rc3</option>
<option value="v7.2.0.rc2">v7.2.0.rc2</option>
<option value="v7.2.0.rc1">v7.2.0.rc1</option>
<option value="v7.1.1">v7.1.1</option>
<option value="v7.1.0">v7.1.0</option>
<option value="v7.1.0.rc1">v7.1.0.rc1</option>
<option value="v7.0.0">v7.0.0</option>
<option value="v7.0.0.rc1">v7.0.0.rc1</option>
<option value="v6.9.2">v6.9.2</option>
<option value="v6.9.1">v6.9.1</option>
<option value="v6.9.0">v6.9.0</option>
<option value="v6.9.0.rc1">v6.9.0.rc1</option>
<option value="v6.8.2">v6.8.2</option>
<option value="v6.8.1">v6.8.1</option>
<option value="v6.8.0">v6.8.0</option>
<option value="v6.8.0.rc1">v6.8.0.rc1</option>
<option value="v6.7.5">v6.7.5</option>
<option value="v6.7.4">v6.7.4</option>
<option value="v6.7.3">v6.7.3</option>
<option value="v6.7.2">v6.7.2</option>
<option value="v6.7.1">v6.7.1</option>
<option value="v6.7.0">v6.7.0</option>
<option value="v6.7.0.rc1">v6.7.0.rc1</option>
<option value="v6.6.5">v6.6.5</option>
<option value="v6.6.4">v6.6.4</option>
<option value="v6.6.3">v6.6.3</option>
<option value="v6.6.2">v6.6.2</option>
<option value="v6.6.1">v6.6.1</option>
<option value="v6.6.0">v6.6.0</option>
<option value="v6.6.0.rc1">v6.6.0.rc1</option>
<option value="v6.6.0.pre1">v6.6.0.pre1</option>
<option value="v6.5.1">v6.5.1</option>
<option value="v6.5.0">v6.5.0</option>
<option value="v6.5.0.rc1">v6.5.0.rc1</option>
<option value="v6.4.3">v6.4.3</option>
<option value="v6.4.2">v6.4.2</option>
<option value="v6.4.1">v6.4.1</option>
<option value="v6.4.0">v6.4.0</option>
<option value="v6.4.0.pre3">v6.4.0.pre3</option>
<option value="v6.4.0.pre2">v6.4.0.pre2</option>
<option value="v6.4.0.pre1">v6.4.0.pre1</option>
<option value="v6.3.1">v6.3.1</option>
<option value="v6.3.0">v6.3.0</option>
<option value="v6.2.4">v6.2.4</option>
<option value="v6.2.3">v6.2.3</option>
<option value="v6.2.2">v6.2.2</option>
<option value="v6.2.1">v6.2.1</option>
<option value="v6.2.0">v6.2.0</option>
<option value="v6.1.0">v6.1.0</option>
<option value="v6.0.2">v6.0.2</option>
<option value="v6.0.1">v6.0.1</option>
<option value="v6.0.0">v6.0.0</option>
<option value="v5.4.2">v5.4.2</option>
<option value="v5.4.1">v5.4.1</option>
<option value="v5.4.0">v5.4.0</option>
<option value="v5.3.0">v5.3.0</option>
<option value="v5.2.1">v5.2.1</option>
<option value="v5.2.0">v5.2.0</option>
<option value="v5.1.0">v5.1.0</option>
<option value="v5.0.1">v5.0.1</option>
<option value="v5.0.0">v5.0.0</option>
<option value="v4.2.0">v4.2.0</option>
<option value="v4.1.0">v4.1.0</option>
<option value="v4.0.0">v4.0.0</option>
<option value="v4.0.0rc2">v4.0.0rc2</option>
<option value="v4.0.0rc1">v4.0.0rc1</option>
<option value="v3.1.0">v3.1.0</option>
<option value="v3.0.3">v3.0.3</option>
<option value="v3.0.2">v3.0.2</option>
<option value="v3.0.1">v3.0.1</option>
<option value="v3.0.0">v3.0.0</option>
<option value="v2.9.1">v2.9.1</option>
<option value="v2.9.0">v2.9.0</option>
<option value="v2.8.2">v2.8.2</option>
<option value="v2.8.1">v2.8.1</option>
<option value="v2.8.0">v2.8.0</option>
<option value="v2.8.0pre">v2.8.0pre</option>
<option value="v2.7.0">v2.7.0</option>
<option value="v2.7.0pre">v2.7.0pre</option>
<option value="v2.6.3">v2.6.3</option>
<option value="v2.6.2">v2.6.2</option>
<option value="v2.6.1">v2.6.1</option>
<option value="v2.6.0">v2.6.0</option>
<option value="v2.6.0pre">v2.6.0pre</option>
<option value="v2.5.0">v2.5.0</option>
<option value="v2.4.2">v2.4.2</option>
<option value="v2.4.1">v2.4.1</option>
<option value="v2.4.0">v2.4.0</option>
<option value="v2.4.0pre">v2.4.0pre</option>
<option value="v2.3.1">v2.3.1</option>
<option value="v2.3.0">v2.3.0</option>
<option value="v2.3.0pre">v2.3.0pre</option>
<option value="v2.2.0">v2.2.0</option>
<option value="v2.2.0pre">v2.2.0pre</option>
<option value="v2.1.0">v2.1.0</option>
<option value="v2.0.0">v2.0.0</option>
<option value="v1.2.2">v1.2.2</option>
<option value="v1.2.1">v1.2.1</option>
<option value="v1.2.0">v1.2.0</option>
<option value="v1.2.0pre">v1.2.0pre</option>
<option value="v1.1.0">v1.1.0</option>
<option value="v1.1.0pre">v1.1.0pre</option>
<option value="v1.0.2">v1.0.2</option>
<option value="v1.0.1">v1.0.1</option>
<option value="v1.0.0">v1.0.0</option>
<option value="v0.9.6">v0.9.6</option>
<option value="v0.9.5">v0.9.5</option>
<option value="v0.9.4">v0.9.4</option></optgroup></select>
<input id="destination" name="destination" type="hidden" value="blob" />
<input id="path" name="path" type="hidden" value="README.md" />
</form>


</div>
<div class='tree-holder' id='tree-holder'>
<ul class='breadcrumb repo-breadcrumb'>
<li>
<i class='fa fa-angle-right'></i>
<a href="/gitlab-org/gitlab-ce/tree/master">gitlab-ce
</a></li>
<li>
<a href="/gitlab-org/gitlab-ce/blob/master/README.md"><strong>
README.md
</strong>
</a></li>
</ul>
<ul class='blob-commit-info well hidden-xs'>
<li class='commit js-toggle-container'>
<div class='commit-row-title'>
<strong class='str-truncated'>
<a class="commit-row-message" href="/gitlab-org/gitlab-ce/commit/b3ab0e4efb6c242cf39805fb8c94c3e6023d15ec">Revert "Change default admin password from "5iveL!fe" to "password""</a>
<a class='text-expander js-toggle-button'>...</a>
</strong>
<div class='pull-right'>
<a class="commit_short_id" href="/gitlab-org/gitlab-ce/commit/b3ab0e4efb6c242cf39805fb8c94c3e6023d15ec">b3ab0e4e</a>
</div>
<div class='notes_count'>
</div>
</div>
<div class='commit-row-description js-toggle-content'>
<pre>&#x000A;This reverts commit <a href="/gitlab-org/gitlab-ce/commit/8a01a1222875b190d32769f7a6e7a74720079d2a" title='Commit: Robert Speicher - Change default admin password from "5iveL!fe" to "password"' class="gfm gfm-commit">8a01a122</a>.</pre>
</div>
<div class='commit-row-info'>
<a class="commit-author-link has_tooltip" data-original-title="sytse@gitlab.com" href="/u/sytses"><img alt="" class="avatar s24" src="https://secure.gravatar.com/avatar/78b060780d36f51a6763ac9831a4f022?s=24&amp;d=identicon" width="24" /> <span class="commit-author-name">Sytse Sijbrandij</span></a>
authored
<div class='committed_ago'>
<time class='time_ago' data-placement='top' data-toggle='tooltip' datetime='2015-07-14T15:59:36Z' title='Jul 14, 2015 3:59pm'>2015-07-14 17:59:36 +0200</time>
<script>$('.time_ago').timeago().tooltip()</script>
 &nbsp;
</div>
<a class="pull-right" href="/gitlab-org/gitlab-ce/tree/b3ab0e4efb6c242cf39805fb8c94c3e6023d15ec">Browse Code »</a>
</div>
</li>

</ul>
<div class='tree-content-holder' id='tree-content-holder'>
<article class='file-holder'>
<div class='file-title'>
<i class="fa fa-file-text-o fa-fw"></i>
<strong>
README.md
</strong>
<small>
5.7 KB
</small>
<div class='file-actions hidden-xs'>
<div class='btn-group tree-btn-group'>
<span class="btn btn-small disabled">Edit</span>
<a class="btn btn-sm" href="/gitlab-org/gitlab-ce/raw/master/README.md" target="_blank">Raw</a>
<a class="btn btn-sm" href="/gitlab-org/gitlab-ce/blame/master/README.md">Blame</a>
<a class="btn btn-sm" href="/gitlab-org/gitlab-ce/commits/master/README.md">History</a>
<a class="btn btn-sm" href="/gitlab-org/gitlab-ce/blob/daed441dfe2bff0c64c581a802ee38a1ac42e6c6/README.md">Permalink</a>
</div>

</div>
</div>
<div class='file-content wiki'>
<h2>&#x000A;<a id="canonical-source" class="anchor" href="#canonical-source" aria-hidden="true"></a>Canonical source</h2>&#x000A;&#x000A;<p>The source of GitLab Community Edition is <a href="https://gitlab.com/gitlab-org/gitlab-ce/">hosted on GitLab.com</a> and there are mirrors to make <a href="/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md">contributing</a> as easy as possible.</p>&#x000A;&#x000A;<h1>&#x000A;<a id="-gitlab" class="anchor" href="#-gitlab" aria-hidden="true"></a><img src="https://about.gitlab.com/images/gitlab_logo.png" alt="logo"> GitLab</h1>&#x000A;&#x000A;<h2>&#x000A;<a id="open-source-software-to-collaborate-on-code" class="anchor" href="#open-source-software-to-collaborate-on-code" aria-hidden="true"></a>Open source software to collaborate on code</h2>&#x000A;&#x000A;<p>To see how GitLab looks please see the <a href="https://about.gitlab.com/features/" rel="nofollow">features page on our website</a>.</p>&#x000A;&#x000A;<ul>&#x000A;<li>Manage Git repositories with fine grained access controls that keep your code secure</li>&#x000A;<li>Perform code reviews and enhance collaboration with merge requests</li>&#x000A;<li>Each project can also have an issue tracker and a wiki</li>&#x000A;<li>Used by more than 100,000 organizations, GitLab is the most popular solution to manage Git repositories on-premises</li>&#x000A;<li>Completely free and open source (MIT Expat license)</li>&#x000A;<li>Powered by <a href="https://github.com/rails/rails" rel="nofollow">Ruby on Rails</a>&#x000A;</li>&#x000A;</ul>&#x000A;&#x000A;<h2>&#x000A;<a id="editions" class="anchor" href="#editions" aria-hidden="true"></a>Editions</h2>&#x000A;&#x000A;<p>There are two editions of GitLab.&#x000A;<em>GitLab <a href="https://about.gitlab.com/features/" rel="nofollow">Community Edition</a> (CE)</em> is available without any costs under an MIT license.</p>&#x000A;&#x000A;<p><em>GitLab Enterprise Edition (EE)</em> includes <a href="https://about.gitlab.com/features/#compare" rel="nofollow">extra features</a> that are most useful for organizations with more than 100 users.&#x000A;To use EE and get official support please <a href="https://about.gitlab.com/pricing/" rel="nofollow">become a subscriber</a>.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="code-status" class="anchor" href="#code-status" aria-hidden="true"></a>Code status</h2>&#x000A;&#x000A;<ul>&#x000A;<li><p><a href="https://ci.gitlab.com/projects/1?ref=master" rel="nofollow"><img src="https://ci.gitlab.com/projects/1/status.png?ref=master" alt="build status"></a> on ci.gitlab.com (master branch)</p></li>&#x000A;<li><p><a href="https://semaphoreapp.com/gitlabhq/gitlabhq" rel="nofollow"><img src="https://semaphoreapp.com/api/v1/projects/2f1a5809-418b-4cc2-a1f4-819607579fe7/243338/badge.png" alt="Build Status"></a></p></li>&#x000A;<li><p><a href="https://codeclimate.com/github/gitlabhq/gitlabhq" rel="nofollow"><img src="https://codeclimate.com/github/gitlabhq/gitlabhq.svg" alt="Code Climate"></a></p></li>&#x000A;<li><p><a href="https://coveralls.io/r/gitlabhq/gitlabhq?branch=master" rel="nofollow"><img src="https://coveralls.io/repos/gitlabhq/gitlabhq/badge.png?branch=master" alt="Coverage Status"></a></p></li>&#x000A;</ul>&#x000A;&#x000A;<h2>&#x000A;<a id="website" class="anchor" href="#website" aria-hidden="true"></a>Website</h2>&#x000A;&#x000A;<p>On <a href="https://about.gitlab.com/" rel="nofollow">about.gitlab.com</a> you can find more information about:</p>&#x000A;&#x000A;<ul>&#x000A;<li><a href="https://about.gitlab.com/subscription/" rel="nofollow">Subscriptions</a></li>&#x000A;<li><a href="https://about.gitlab.com/consultancy/" rel="nofollow">Consultancy</a></li>&#x000A;<li><a href="https://about.gitlab.com/community/" rel="nofollow">Community</a></li>&#x000A;<li>&#x000A;<a href="https://about.gitlab.com/gitlab-com/" rel="nofollow">Hosted GitLab.com</a> use GitLab as a free service</li>&#x000A;<li>&#x000A;<a href="https://about.gitlab.com/gitlab-ee/" rel="nofollow">GitLab Enterprise Edition</a> with additional features aimed at larger organizations.</li>&#x000A;<li>&#x000A;<a href="https://about.gitlab.com/gitlab-ci/" rel="nofollow">GitLab CI</a> a continuous integration (CI) server that is easy to integrate with GitLab.</li>&#x000A;</ul>&#x000A;&#x000A;<h2>&#x000A;<a id="requirements" class="anchor" href="#requirements" aria-hidden="true"></a>Requirements</h2>&#x000A;&#x000A;<p>GitLab requires the following software:</p>&#x000A;&#x000A;<ul>&#x000A;<li>Ubuntu/Debian/CentOS/RHEL</li>&#x000A;<li>Ruby (MRI) 2.0 or 2.1</li>&#x000A;<li>Git 1.7.10+</li>&#x000A;<li>Redis 2.0+</li>&#x000A;<li>MySQL or PostgreSQL</li>&#x000A;</ul>&#x000A;&#x000A;<p>Please see the <a href="/gitlab-org/gitlab-ce/blob/master/doc/install/requirements.md">requirements documentation</a> for system requirements and more information about the supported operating systems.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="installation" class="anchor" href="#installation" aria-hidden="true"></a>Installation</h2>&#x000A;&#x000A;<p>The recommended way to install GitLab is using the provided <a href="https://about.gitlab.com/downloads/" rel="nofollow">Omnibus packages</a>. Compared to an installation from source, this is faster and less error prone. Just select your operating system, download the respective package (Debian or RPM) and install it using the system's package manager.</p>&#x000A;&#x000A;<p>There are various other options to install GitLab, please refer to the <a href="https://about.gitlab.com/installation/" rel="nofollow">installation page on the GitLab website</a> for more information.</p>&#x000A;&#x000A;<p>You can access a new installation with the login <strong><code>root</code></strong> and password <strong><code>5iveL!fe</code></strong>, after login you are required to set a unique password.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="third-party-applications" class="anchor" href="#third-party-applications" aria-hidden="true"></a>Third-party applications</h2>&#x000A;&#x000A;<p>There are a lot of <a href="https://about.gitlab.com/applications/" rel="nofollow">third-party applications integrating with GitLab</a>. These include GUI Git clients, mobile applications and API wrappers for various languages.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="gitlab-release-cycle" class="anchor" href="#gitlab-release-cycle" aria-hidden="true"></a>GitLab release cycle</h2>&#x000A;&#x000A;<p>Since 2011 a minor or major version of GitLab is released on the 22nd of every month. Patch and security releases are published when needed. New features are detailed on the <a href="https://about.gitlab.com/blog/" rel="nofollow">blog</a> and in the <a href="/gitlab-org/gitlab-ce/blob/master/CHANGELOG">changelog</a>. For more information about the release process see the <a href="https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/release">release documentation</a>. Features that will likely be in the next releases can be found on the <a href="http://feedback.gitlab.com/forums/176466-general" rel="nofollow">feature request forum</a> with the status <a href="http://feedback.gitlab.com/forums/176466-general/status/796456" rel="nofollow">started</a> and <a href="http://feedback.gitlab.com/forums/176466-general/status/796457" rel="nofollow">completed</a>.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="upgrading" class="anchor" href="#upgrading" aria-hidden="true"></a>Upgrading</h2>&#x000A;&#x000A;<p>For upgrading information please see our <a href="https://about.gitlab.com/update/" rel="nofollow">update page</a>.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="install-a-development-environment" class="anchor" href="#install-a-development-environment" aria-hidden="true"></a>Install a development environment</h2>&#x000A;&#x000A;<p>To work on GitLab itself, we recommend setting up your development environment with <a href="https://gitlab.com/gitlab-org/gitlab-development-kit">the GitLab Development Kit</a>.&#x000A;If you do not use the GitLab Development Kit you need to install and setup all the dependencies yourself, this is a lot of work and error prone.&#x000A;One small thing you also have to do when installing it yourself is to copy the example development unicorn configuration file:</p>&#x000A;<pre class="code highlight white plaintext"><code>cp config/unicorn.rb.example.development config/unicorn.rb&#x000A;</code></pre>&#x000A;&#x000A;<p>Instructions on how to start GitLab and how to run the tests can be found in the <a href="https://gitlab.com/gitlab-org/gitlab-development-kit#development">development section of the GitLab Development Kit</a>.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="documentation" class="anchor" href="#documentation" aria-hidden="true"></a>Documentation</h2>&#x000A;&#x000A;<p>All documentation can be found on <a href="http://doc.gitlab.com/ce/" rel="nofollow">doc.gitlab.com/ce/</a>.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="getting-help" class="anchor" href="#getting-help" aria-hidden="true"></a>Getting help</h2>&#x000A;&#x000A;<p>Please see <a href="https://about.gitlab.com/getting-help/" rel="nofollow">Getting help for GitLab</a> on our website for the many options to get help.</p>&#x000A;&#x000A;<h2>&#x000A;<a id="is-it-any-good" class="anchor" href="#is-it-any-good" aria-hidden="true"></a>Is it any good?</h2>&#x000A;&#x000A;<p><a href="https://news.ycombinator.com/item?id=3067434" rel="nofollow">Yes</a></p>&#x000A;&#x000A;<h2>&#x000A;<a id="is-it-awesome" class="anchor" href="#is-it-awesome" aria-hidden="true"></a>Is it awesome?</h2>&#x000A;&#x000A;<p>Thanks for <a href="https://twitter.com/supersloth/status/489462789384056832" rel="nofollow">asking this question</a> Joshua.&#x000A;<a href="https://twitter.com/gitlab/favorites" rel="nofollow">These people</a> seem to like it.</p>
</div>

</article>
</div>

</div>

</div>
</div>
</div>
</div>
</div>

<script>
  GitLab.GfmAutoComplete.dataSource = "/gitlab-org/gitlab-ce/autocomplete_sources?type=NilClass&type_id=master%2FREADME.md"
  GitLab.GfmAutoComplete.setup();
</script>


</body>
</html>